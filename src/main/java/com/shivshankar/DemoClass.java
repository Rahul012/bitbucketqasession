package com.shivshankar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DemoClass {

    public static void main(String args[]) {

        WebDriver driver;

        System.setProperty( "webdriver.chrome.driver", "C:\\Program Files\\chromedriver.exe" );
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait( 20, TimeUnit.SECONDS );
        driver.get( "http://learn-automation.com/" );

    }
}

